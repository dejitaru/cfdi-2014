<?php 
set_time_limit(0);
ini_set('memory_limit', '100M');
ini_set('post_max_size', '20M');
ini_set('upload_max_filesize', '20M');

include_once('c:\\xampp\\htdocs\\envios\\config.php');
$db = new PDO("sqlite:data.db");

if( isset($_POST['btn_upload']) )
{
	$new_cvs = upload( $CONFIG['csv_path'] );
	if( $new_cvs)
	{
		truncate_csv();
		insert_csv($new_cvs);
		csvToSql(1);
		$files = glob('c:\\xampp\\htdocs\\envios\\tmp\\*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}		
	}
	else
	{
		echo 'Archivi no subido';
	}
}

function truncate_csv()
{
	global $db;
	$db->exec('delete from csvs');
	
}
function insert_csv($file)
{
	global $db;
	global $CONFIG;
	$lines = file($CONFIG['csv_path'].$file);
	$total= count($lines);// - 1; // minus 1 because of header
	$created_at = $updated_at = date('Y-m-d H:i:s'); 
	$message = 'NULL';
	$status = 'nuevo';
	$query = 'INSERT INTO csvs (file,records,status,message,created_at,updated_at) VALUES ("'.$file.'",'.$total.',"'.$status.'",'.$message.',"'.$created_at.'","'.$updated_at.'")';
	$db->exec($query);
}
function upload($path = '/')
{
	$origin = $_FILES['cvs_file']['tmp_name'];
	$destination = $path.$_FILES['cvs_file']['name'];
	if( move_uploaded_file($origin,$destination) )
	{
		return $_FILES['cvs_file']['name'];
	}
	else
	{
		if($_FILES['cvs_file']['error'] == UPLOAD_ERR_INI_SIZE) {
        echo 'File size exceeded maximum PHP upload file size.';
		}
		return FALSE;
	}
}
function csvToSql($id)
{
	global $db;
	global $CONFIG;
	$csv = $db->query('SELECT * FROM csvs where id='.$id)->fetch();
	$row = 0;
	if (($handle = fopen($CONFIG['csv_path'].$csv['file'], "r")) !== FALSE) 
	{
		$query = 'Delete from temp_records';
		$db->exec($query);	
		
	    while (($data = fgetcsv($handle, 5000, "|")) !== FALSE) 
	    {
	        $row++;
			//echo $row.': '.$data[0].'<br/>'; 
			//if($row==4){ exit;}
	        //if($row==1){ continue;} //jump first line because is the header
	        $numero_trabajador 	= $data[13];
	        $email 				= $data[2]; 
	        $ruta_pdf 			= $data[0];
	        $ruta_xml 			= $data[1];
	        $status 			= 'NUEVO'; 
	        $zip_done			= 0;
	        $email_done			= 0;
	        $created_at 		= date('Y-m-d H:i:s'); 
	        $query = 'INSERT INTO temp_records(numero_trabajador,email,ruta_pdf,ruta_xml,status,zip_done,email_done,created_at) values("'.$numero_trabajador.'","'.$email.'","'.$ruta_pdf.'","'.$ruta_xml.'","'.$status.'","'.$zip_done.'","'.$email_done.'","'.$created_at.'")';
	        $db->exec($query);
	    }
	    fclose($handle);	    
	}
	else
	{
		echo 'El archivo no se encontró';
		exit;
	}
}

$csvcs = $db->query('SELECT * FROM csvs');
$temp_records = $db->query('SELECT count(*) total FROM temp_records')->fetch();
$zips = $db->query('SELECT count(*) total FROM temp_records where zip_done=1')->fetch();
$emails = $db->query('SELECT count(*) total FROM temp_records where email_done=1')->fetch();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <title>Envíos CFDI versión 1.0 - 15 abril 2014</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- CSS code from Bootply.com editor -->
        <style type="text/css">
            body {
  				padding-top: 50px;
			}
			a{
				color: #000;
			}
        </style>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    </head>
    <!-- HTML code from Bootply.com editor -->
    <body  >
        
        <div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!--a class="navbar-brand" href="#">Brand</a-->
    </div>
    <div class="collapse navbar-collapse">
    	<p class="navbar-text">Envíos CFDI versión 1.0</p>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div class="container">
	<form class="form-inline" role="form" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label class="sr-only" for="exampleInputEmail2">Archivo (.csv) </label>
			<input class="btn btn-xs" type="file" name="cvs_file">
		</div>
		<div class="form-group">
			<button class="btn btn-xs" name="btn_upload" value="btn_upload">Subir archivo</button>
		</div>
	</form>

	<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Fecha</th>
				<th>Archivo</th>
				<th>Registros</th>
				<th>Zips</th>
				<th>Envíos</th>
				<th></th>
				<th>Status</th>
				<th>Mensaje</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php while ( $csv = $csvcs->fetch() ):?>
			<tr>
				<td><?php echo $csv['id'];?></td>
				<td><?php echo $csv['created_at'];?></td>
				<td><?php echo $csv['file'];?></td>
				<td><?php echo $csv['records'];?></td>
				<td class="total_zips"><?php echo $zips['total']?></td>
				<td class="total_emails"><?php echo $emails['total']?></td>
				<td><a class="btn btn-primary btn-sm procesar" id="<?php echo $csv['id']?>" href="procesar.php?id=<?php echo $csv['id']?>" class="btn btn-xs">Procesar</a></td>
				<td><span class="label label-warning status"><?php echo $csv['status'];?></span></td>
				<td><?php echo $csv['message'];?></td>
				<td><a href="/delete.php?id=<?php echo $csv['id']?>"><i class="fa fa-trash-o"></i></a></td>
			</tr>
			<?php endwhile;
			?>
		</tbody>
	
	</table>
</div><!-- /.container -->
        
        <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


        <script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>





        
        <!-- JavaScript jQuery code from Bootply.com editor -->
        
        <script type='text/javascript'>
        var proceso = '';
        var proceso_emails = '';
        
        $(document).ready(function() {
        	$('.procesar').on('click',function(event)
        	{
        		event.preventDefault();
        		var element = this;        		
        		var id = $(element).attr('id'); 
        		
        		$.post('procesar.php?id='+id,function()
        		{
        			$(element).html('Procesando');
        			$(element).parent().parent().find('.status').html('Creando Zips');
        			proceso  = setInterval("updateZips()",1000);
        		});
        		
        		
        	});
        
            
        
        });
        
        function updateZips()
        {
        	$.post('info.php?q=zips',function(data)
        	{
        		data = JSON.parse(data);
        		if(data.error=="1")
        		{ 
        			clearInterval(proceso);
					$('.total_zips').html(data.total);
        			sendEmails();	
        		}
        		else
        		{
        			$('.total_zips').html(data.total);
        		}
        	});
        }
        function sendEmails()
        {	
        	proceso_emails  = setInterval("updateEmails()",3000);
        	$.post('sendEmails.php',function(data)
        	{
        		
        	});
        }
        function updateEmails()
        {
        	$.post('info.php?q=emails',function(data)
        	{
        		data = JSON.parse(data);
        		if( data.error == "1" )
        		{ 
        			clearInterval(proceso_emails);
        		}
        		else
        		{
        			$('.total_emails').html(data.total);
        		}
        	});
        }
        
        </script>        
    </body>
</html>